USE ProcessoSeletivoTODOS
GO

IF OBJECT_ID('todos.sp_insert_usuario') is not null drop proc todos.sp_insert_usuario;
GO

CREATE PROC todos.sp_insert_usuario
	 @login AS VARCHAR(120)
	,@email AS VARCHAR(220)
	,@senha AS VARCHAR(220)
	,@nome AS VARCHAR(120)
	,@ativo AS BIT = 1
AS
BEGIN	
		
	IF @login = '' OR @login IS NULL 
	BEGIN
		THROW 51000, 'O par�metro @login n�o pode estar nulo ou vazio',1;
	END	
		
	IF @email = '' OR @email IS NULL 
	BEGIN
		THROW 51000, 'O par�metro @email n�o pode estar nulo ou vazio',1;
	END
	
	IF @senha = '' OR @senha IS NULL 
	BEGIN
		THROW 51000, 'O par�metro @senha n�o pode estar nulo ou vazio',1;
	END
	
	INSERT INTO todos.USUARIO
			( [LOGIN],NOME,  EMAIL, SENHA, ATIVO, DT_INCLUSAO)
			VALUES
			( @login, @nome, @email, @senha, @ativo, GETDATE() );
END
GO

EXEC todos.sp_insert_usuario
	 @login = 'loginxcript'
	,@email = 'email@login.com'
	,@senha = 'senha'
	,@nome = 'Ramon Costa'
	,@ativo = DEFAULT
	
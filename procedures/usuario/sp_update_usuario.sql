use ProcessoSeletivoTODOS
go

IF OBJECT_ID('todos.sp_update_usuario') is not null drop proc todos.sp_update_usuario;
GO

CREATE PROC todos.sp_update_usuario
	 @usuarioid as int
	,@login AS VARCHAR(120)
	,@nome as VARCHAR(120)
	,@email AS VARCHAR(220)
	,@senha AS VARCHAR(220)
	,@ativo AS BIT = 1
AS
BEGIN	
		
	IF @login = '' OR @login IS NULL 
	BEGIN
		THROW 51000, 'O par�metro @login n�o pode estar nulo ou vazio',1;
	END	
		
	IF @email = '' OR @email IS NULL 
	BEGIN
		THROW 51000, 'O par�metro @email n�o pode estar nulo ou vazio',1;
	END
	
	IF @senha = '' OR @senha IS NULL 
	BEGIN
		THROW 51000, 'O par�metro @senha n�o pode estar nulo ou vazio',1;
	END
	
	IF NOT EXISTS(SELECT 1 FROM todos.USUARIO where ID_USUARIO = @usuarioid) 
	BEGIN
		THROW 51000, 'N�o foi encontrado nem um usu�rio para o id informado ;',1;
	END 
	UPDATE todos.USUARIO
		SET  [LOGIN] = @login
			,[EMAIL] = @email
			,[SENHA] = @senha
			,[ATIVO] = @ativo
			,[NOME] = @nome
		WHERE ID_USUARIO = @usuarioid
END
GO

EXEC todos.sp_update_usuario
	 @login = 'newlogin'
	,@usuarioid = 5
	,@email = 'email@email.com'
	,@senha = 'senha'
	,@nome = 'Novo Login'
	,@ativo = DEFAULT
	

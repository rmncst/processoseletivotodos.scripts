use ProcessoSeletivoTODOS
go

IF OBJECT_ID('todos.sp_delete_usuario') is not null drop proc todos.sp_delete_usuario;
GO

CREATE PROC todos.sp_delete_usuario
	 @usuarioid as int
AS
BEGIN	
	DECLARE @CONTROWS AS INT;
	
	IF NOT EXISTS(SELECT 1 FROM todos.USUARIO where ID_USUARIO = @usuarioid) 
	BEGIN
		THROW 51000, 'N�o foi encontrado nem um usu�rio para o id informado;',1;
	END 
	
	IF EXISTS ( SELECT 1 FROM todos.USUARIO_PERFIL WHERE ID_USUARIO = @usuarioid)
	BEGIN
		THROW 51000, 'Este usu�rio t�m v�nculos com perfis ativos. Caso deseje excluir, exclua todos os v�nculos com perfis primeiro.', 1;
	END
	
	DELETE FROM todos.USUARIO WHERE ID_USUARIO = @usuarioid;	
	
END
GO

EXEC todos.sp_delete_usuario
	 @usuarioid = 5
	
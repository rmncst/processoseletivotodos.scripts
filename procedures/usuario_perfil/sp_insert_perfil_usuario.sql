USE ProcessoSeletivoTODOS
GO

IF OBJECT_ID('todos.sp_insert_perfil_usuario') is not null drop proc todos.sp_insert_perfil_usuario;
GO

CREATE PROC todos.sp_insert_perfil_usuario
	 @id_perfil AS INT
	,@id_usuario AS INT
	,@ativo AS BIT = 1
AS
BEGIN	
	IF NOT EXISTS (SELECT 1 FROM todos.PERFIL WHERE ID_PERFIL = @id_perfil )
	BEGIN
		THROW 51000, 'O perfil informado n�o existe' , 1;
	END

	IF NOT EXISTS (SELECT 1 FROM todos.USUARIO WHERE ID_USUARIO = @id_usuario)
	BEGIN
		THROW 51000, 'O usu�rio informado n�o existe', 1;
	END 
	
	IF EXISTS (SELECT 1 FROM todos.USUARIO_PERFIL WHERE ID_USUARIO = @id_usuario and ID_PERFIL = @id_perfil)
	BEGIN
		THROW 51000, 'Os dados inseridos j� est�o cadastrados', 1;
	END 

	INSERT INTO todos.USUARIO_PERFIL
		( ID_USUARIO , ID_PERFIL, ATIVO )
	VALUES
		( @id_usuario , @id_perfil , @ativo );
END
GO

EXEC todos.sp_insert_perfil_usuario
	 @id_perfil = 1
	,@id_usuario = 2
	,@ativo = DEFAULT


	
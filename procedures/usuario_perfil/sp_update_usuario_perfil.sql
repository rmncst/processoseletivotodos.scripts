USE ProcessoSeletivoTODOS
go

IF OBJECT_ID('todos.sp_update_perfil_usuario') is not null drop proc todos.sp_update_perfil_usuario;
GO

CREATE PROC todos.sp_update_perfil_usuario
	 @id_perfil AS INT
	,@id_usuario AS INT
	,@ativo AS BIT = 1
AS
BEGIN	
	
	IF NOT EXISTS (SELECT 1 FROM todos.USUARIO_PERFIL WHERE ID_USUARIO = @id_usuario and ID_PERFIL = @id_perfil)
	BEGIN
		THROW 51000, 'Os dados n�o existem na tabela de usuario perfil', 1;
	END 

	UPDATE todos.USUARIO_PERFIL
		SET ATIVO = @ativo
		WHERE ID_USUARIO = @id_usuario AND ID_PERFIL = @id_perfil
END
GO

EXEC todos.sp_update_perfil_usuario
	 @id_perfil = 1
	,@id_usuario = 2
	,@ativo = 0
	

	--select * from todos.USUARIO_PERFIL
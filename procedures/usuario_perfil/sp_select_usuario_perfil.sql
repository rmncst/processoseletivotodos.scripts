USE ProcessoSeletivoTODOS
GO

if OBJECT_ID('todos.sp_select_usuario_perfil','P') is not null DROP PROC todos.sp_select_usuario_perfil;
GO

create proc todos.sp_select_usuario_perfil 
	 @predicate as varchar(120) = null,
	 @order as varchar(120) = null,
	 @top as int = null
as
begin
	DECLARE @sql as nvarchar(max) = 'SELECT {TOP} u.* , p.* FROM 
										(SELECT * FROM todos.USUARIO_PERFIL {PREDICATE}) UF 
											JOIN todos.USUARIO u on u.ID_USUARIO = UF.ID_USUARIO
											JOIN todos.perfil p on p.ID_PERFIL = UF.ID_PERFIL	
											{ORDER} ';

	if @predicate is not null
	begin
		set @sql = REPLACE(@sql,'{PREDICATE}', 'WHERE ' + @predicate);
	end
	else begin
		set @sql = REPLACE(@sql,'{PREDICATE}', '');
	end
	
	if @order is not null begin
		set @sql = REPLACE(@sql, '{ORDER}', 'ORDER BY ' + @order);
	end
	else begin
		set @sql = REPLACE(@sql, '{ORDER}', '');
	end 

	if @top is not null begin
		set @sql = REPLACE(@sql, '{TOP}', 'TOP ' + cast(@top as varchar(12)));
	end
	else begin
		set @sql = REPLACE(@sql, '{TOP}', '');
	end 

	PRINT @sql
	exec(@sql);
end
GO

exec todos.sp_select_usuario_perfil 
	@predicate = 'ID_USUARIO = 1'
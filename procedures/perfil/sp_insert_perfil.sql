USE ProcessoSeletivoTODOS
go

IF OBJECT_ID('todos.sp_insert_perfil') is not null drop proc todos.sp_insert_perfil;
GO

CREATE PROC todos.sp_insert_perfil
	 @nome AS VARCHAR(120)
AS
BEGIN	
		
	IF @nome = '' OR @nome IS NULL 
	BEGIN
		THROW 51000, 'O par�metro @nome n�o pode estar nulo ou vazio',1;
	END
	
	INSERT INTO todos.PERFIL
			( [NOME] )
		VALUES
			( @nome );
END
GO

EXEC todos.sp_insert_perfil
	 @nome = 'new profile'


	 
	

	
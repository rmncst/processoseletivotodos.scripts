USE ProcessoSeletivoTODOS
go


IF OBJECT_ID('todos.sp_delete_perfil') is not null drop proc todos.sp_delete_perfil;
GO

CREATE PROC todos.sp_delete_perfil
	  @idperfil AS INT
AS
BEGIN	

	IF NOT EXISTS ( SELECT 1 FROM todos.PERFIL where ID_PERFIL = @idperfil )
	BEGIN
		THROW 51000, 'O perfil informado n�o existe', 1;
	END 

	IF EXISTS ( SELECT 1 FROM todos.USUARIO_PERFIL where ID_PERFIL = @idperfil )
	BEGIN
		THROW 51000, 'o perfil informado possue v�nculos ativos com usu�rios. Para exclu�-lo, exclua os v�nculos primeiros.',1;
	END 
	
	DELETE FROM todos.PERFIL WHERE ID_PERFIL = @idperfil;
END
GO

EXEC todos.sp_delete_perfil
	  @idperfil = 1

	  
	 
	

	
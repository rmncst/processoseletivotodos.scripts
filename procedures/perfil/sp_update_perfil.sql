USE ProcessoSeletivoTODOS
go

IF OBJECT_ID('todos.sp_update_perfil') is not null drop proc todos.sp_update_perfil;
GO

CREATE PROC todos.sp_update_perfil
	  @nome AS VARCHAR(120)
	 ,@idperfil AS INT
AS
BEGIN	
		
	IF @nome = '' OR @nome IS NULL 
	BEGIN
		THROW 51000, 'O par�metro @nome n�o pode estar nulo ou vazio',1;
	END

	IF NOT EXISTS ( SELECT 1 FROM todos.PERFIL where ID_PERFIL = @idperfil )
	BEGIN
		THROW 51000, 'O perfil informado n�o existe', 1;
	END 
	
	UPDATE todos.PERFIL
			SET [NOME] = @nome
		WHERE ID_PERFIL = @idperfil;
END
GO

EXEC todos.sp_update_perfil
	  @nome = 'new profile update'
	 ,@idperfil = 1


	 
	

	
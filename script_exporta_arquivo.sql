--===========================================================================================================================================================
-- � NECESS�RIO ALTERAR O VALOR DA VARI�VEL  @pathDestino para um diret�rio existente e com permiss�o para o usu�rio do mssql criar arquivo e deletar arquivo
---==========================================================================================================================================================

USE ProcessoSeletivoTODOS;
go

EXEC sp_configure 'show advanced options', 1;  
GO  
-- To update the currently configured value for advanced options.  
RECONFIGURE;  
GO  
-- To enable the feature.  
EXEC sp_configure 'xp_cmdshell', 1;  
GO  
-- To update the currently configured value for this feature.  
RECONFIGURE;  
GO  

IF OBJECT_ID('tempdb..#TABLE_EXPORT') IS NOT NULL DROP TABLE #TABLE_EXPORT;
GO

DECLARE @pathDestino as VARCHAR(500) = 'D:\ti\vsProjects\Todos.Processo.Seletivo\arquivo_exportado.txt';
DECLARE @commandInicial as VARCHAR(500) = 'del ' + @pathDestino;

DECLARE @tam_nome AS INT, @tam_email as int, @tam_login as int, @command as varchar(400);

SELECT @tam_nome = MAX(LEN(NOME))  from todos.USUARIO
SELECT @tam_email = MAX(LEN(EMAIL))  from todos.USUARIO
SELECT @tam_login = MAX(LEN(LOGIN))  from todos.USUARIO

SELECT CONCAT('echo ',LOGIN, REPLICATE(' ' , @tam_login - len(login)),'  ', NOME,REPLICATE(' ', @tam_nome - LEN(NOME) ), '  ', EMAIL, REPLICATE(' ', @tam_email - len(email)),' >> ', @pathDestino) AS COMMAND
	INTO #TABLE_EXPORT
FROM todos.USUARIO;

exec master..xp_cmdshell @commandInicial;

DECLARE cr_export CURSOR FOR 
	SELECT * FROM #TABLE_EXPORT;

OPEN cr_export
FETCH NEXT FROM cr_export INTO @command
WHILE @@FETCH_STATUS = 0
BEGIN
	exec master..xp_cmdshell @command;
FETCH NEXT FROM cr_export INTO @command
END 

close cr_export;
deallocate cr_export;


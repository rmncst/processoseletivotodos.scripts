use ProcessoSeletivoTODOS
go

--==================================================================
-- Autor: Ramon Costa
-- Descri��o: IMPORTA DADOS DE UM DETERMINADO ARQUIVO TEXTO
-- Observa��es: A compatibilidade deste script vai at� o mssql2008 | Alterar o path do arquivo TXT ao baixar o arquivo.
--===================================================================

IF OBJECT_ID('tempdb..#TEMPINSERT') IS NOT NULL DROP TABLE tempdb..#TEMPINSERT;
GO

CREATE TABLE #TEMPINSERT ( TEMP_ROW VARCHAR(MAX) );
DECLARE @cur_row as VARCHAR(MAX) , @login as varchar(120) , @nome as varchar(120), @email as varchar(220);

BULK INSERT #TEMPINSERT
   FROM 'D:\ti\vsProjects\Todos.Processo.Seletivo\importa.txt'
   WITH 
      (
         ROWTERMINATOR ='\n'
      );

DECLARE cr_arquivotxt CURSOR FOR
	SELECT * FROM #TEMPINSERT

OPEN cr_arquivotxt
FETCH NEXT FROM cr_arquivotxt INTO @cur_row

WHILE @@FETCH_STATUS = 0
BEGIN
	set @cur_row = ltrim(rtrim(@cur_row));

	SET @login = SUBSTRING(@cur_row, 0 , CHARINDEX(' ',@cur_row));
	PRINT @login;

	DECLARE @cur_row_reverse as varchar(max) = reverse(@cur_row);
	SET @email = REVERSE(SUBSTRING(@cur_row_reverse, 0 , CHARINDEX(' ',@cur_row_reverse)));
	PRINT @email;
	
	DECLARE @charindexmail as int = LEN(@cur_row_reverse) - len(@email);
	DECLARE @charindex as int = LEN(@login) + 1;
	SET @nome = LTRIM(RTRIM(SUBSTRING(@cur_row , @charindex , @charindexmail - @charindex)));
	PRINT @nome;

	INSERT INTO ProcessoSeletivoTODOS.todos.USUARIO( [LOGIN],NOME,EMAIL,ATIVO,SENHA,DT_INCLUSAO)
		VALUES ( @login,@nome,@email,1,'DefaultSenha',getdate());
	
	PRINT '';
FETCH NEXT FROM cr_arquivotxt INTO @cur_row
END

CLOSE cr_arquivotxt
DEALLOCATE cr_arquivotxt;
GO

SELECT TOP 4 * FROM ProcessoSeletivoTODOS.todos.USUARIO ORDER BY ID_USUARIO DESC;

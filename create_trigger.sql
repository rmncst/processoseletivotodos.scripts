USE ProcessoSeletivoTODOS
go

IF OBJECT_ID ('todos.trg_auditoria_usuario', 'TR') IS NOT NULL DROP TRIGGER todos.trg_auditoria_usuario;  
GO

CREATE TRIGGER todos.trg_auditoria_usuario
	ON todos.Usuario
AFTER INSERT, DELETE, UPDATE
AS  
BEGIN	
	IF EXISTS (SELECT 1 FROM inserted)
	BEGIN
		IF EXISTS (SELECT 1 FROM deleted)
		BEGIN		
			INSERT INTO todos.OPERACAO_USUARIO
				( 
					 DT_LOG
					,ID_USUARIO
					,TIPO 
				)
			SELECT GETDATE()
				  ,ID_USUARIO
				  ,'U'
				FROM inserted	
		END ELSE
		BEGIN
			INSERT INTO todos.OPERACAO_USUARIO
				( 
					 DT_LOG
					,ID_USUARIO
					,TIPO 
				)
			SELECT GETDATE()
				  ,ID_USUARIO
				  ,'I'
				FROM inserted	
		END
	END 

	IF EXISTS (SELECT 1 FROM deleted)
	BEGIN	
		IF NOT EXISTS (SELECT 1 FROM inserted)
		begin
			INSERT INTO todos.OPERACAO_USUARIO
				( 
					 DT_LOG
					,ID_USUARIO
					,TIPO 
				)
			SELECT GETDATE()
					,ID_USUARIO
					,'D'
				FROM deleted	
		end
	END
		
END 
